package de.haufe.hzm;

import de.haufe.hzm.hxif.HxIF1WebServiceClient;
import de.haufe.hzm.hxif.MimeTypeEncoding;
import de.haufe.hzm.hxif.WebServiceClientException;

import java.io.*;
import java.util.Arrays;
import java.util.List;

/**
 * This client is an example of how to use {@link de.haufe.hzm.hxif.HxIF1WebServiceClient} to
 * interact with the API of the Haufe Zeugnis Manager API for transmitting CoreData.
 */
public class HxIFClient {

    public static void main(final String[] args) throws WebServiceClientException, IOException {
		if (args == null || args.length < 2) {
			printUsage();
			return;
		}
		System.setProperty("jsse.enableSNIExtension", "false");
        HxIF1WebServiceClient client = new HxIF1WebServiceClient(
			readArgument(args, Arrays.asList("--host")),
			readArgument(args, Arrays.asList("--username", "-u")),
			readArgument(args, Arrays.asList("--password", "-p")));

		final String basicAuthUsername = readArgument(args, Arrays.asList("--basic-auth-username", "--ba-username"));
		final String basicAuthPassword = readArgument(args, Arrays.asList("--basic-auth-password", "--ba-password"));
		// if both are given, then configure the client for basic authentication.
		if (basicAuthUsername != null && basicAuthPassword != null) {
			client.setBasicAuth(basicAuthUsername, basicAuthPassword);
		}
		client.trustAnyCertificate();

        System.out.println("Perform a single employee data transmission:");
        InputStream response = client.transmitEmployees(
                MimeTypeEncoding.XML,
                new FileInputStream(readArgument(args, Arrays.asList("--file", "-f"))),
                MimeTypeEncoding.XML);
        printStreamToSystemOut(response);

        System.out.println("Request the current status:");
        printStreamToSystemOut(client.requestStatus());
    }

	public static void printUsage() {
		System.out.println(
			"Usage is: java -jar <jarfile> [OPTIONS]\n" +
			"Options are:\n" +
			"    --host        (mandatory) Server you want to contact\n" +
			"    --username -u (mandatory) Username used by the client to sign in the API\n" +
			"    --password -p (mandatory) Password used by the client to sign in the API\n" +
			"    --file -f     (mandatory) File to be uploaded\n" +
			"    --basic-auth-username --ba-username (optional) Username used to bypass the basic authentication\n" +
			"    --basic-auth-password --ba-password (optional) Password used to bypass the basic authentication\n"
		);
	}

	/**
	 * This is a helper method to extract an argument from the given ones.
	 */
	private static String readArgument(String[] args, List<String> names) {
		for (int i = 0; i < args.length; i++) {
			// check whether it matches the name
			if (names.contains(args[i])) {
				// the following argument might be the value for it.
				if (i + 1 < args.length) {
					return args[i + 1];
				}
			}
		}
		return null; // we did not find the requested argument
	}

	/**
	 * This is just a helper method to print the responses to System.out
	 */
    public static void printStreamToSystemOut(InputStream stream) throws IOException {
        // read the status response.
        BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
        String line;
        while ((line = reader.readLine()) != null) {
            System.out.println("  " + line);
        }
    }
}
