package de.haufe.hzm.hxif;

/**
 * This exception is thrown on any error the web client expires.
 */
public class WebServiceClientException extends Exception {

    public WebServiceClientException(String message, String reason) {
        super(message + ":" + reason);
    }

    public WebServiceClientException(String message, Throwable cause) {
        super(message, cause);
    }
}
