package de.haufe.hzm.hxif;

/**
 * Those are all currently supported encodings.
 */
public enum MimeTypeEncoding {
    JSON("application/json"),
    XML("application/xml");

    private final String mineType;

    MimeTypeEncoding(String mineType) {
        this.mineType = mineType;
    }

    public String getMineType() {
        return mineType;
    }
}
