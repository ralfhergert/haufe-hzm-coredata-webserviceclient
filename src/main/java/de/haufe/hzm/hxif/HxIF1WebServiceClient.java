package de.haufe.hzm.hxif;

import javax.net.ssl.*;
import java.io.*;
import java.net.*;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

/**
 * This WebService Client uses the HZM HxIF Interface in version 1.
 */
public class HxIF1WebServiceClient {

	private static final String protocol = "https";
    private final String serverHost; // host you want to contact.
    private final String username; // username, please ask Haufe for credentials
    private final String password; // corresponding password.

    private String authCookie = null;
    private String basicAuth = null;

    public HxIF1WebServiceClient(String serverHost, String username, String password) {
        this.serverHost = serverHost;
        this.username = username;
        this.password = password;
    }

	public boolean trustAnyCertificate() {
		X509TrustManager tm = new X509TrustManager() {
			@Override
			public X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			@Override
			public void checkServerTrusted(X509Certificate[] paramArrayOfX509Certificate, String paramString) throws CertificateException {}

			@Override
			public void checkClientTrusted(X509Certificate[] paramArrayOfX509Certificate, String paramString) throws CertificateException {}
		};

		try {
			SSLContext ctx = SSLContext.getInstance("TLS");
			ctx.init(null, new TrustManager[]{tm}, null);
			SSLContext.setDefault(ctx);
			HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
				@Override
				public boolean verify(String s, SSLSession sslSession) {
					return true;
				}
			});
			return true;
		} catch (NoSuchAlgorithmException|KeyManagementException e) {
			return false;
		}
	}

    /**
     * This method sets the basic authentication with the given username and password.
     * If username or password is null, then no basicAuthentication will be used.
     */
    public void setBasicAuth(String username, String password) {
        if (username == null || password == null) {
            basicAuth = null; // no basic authentication shall be used.
        } else {
            basicAuth = new sun.misc.BASE64Encoder().encode((username + ":" + password).getBytes());
        }
    }

    /**
     * This method performs the request to login and retrieve the authentication cookie for following requests.
     */
    public void login() throws WebServiceClientException {
        try {
            HttpURLConnection connection = (HttpURLConnection) new URL(protocol + "://" + serverHost + "/hxif/v1/login").openConnection();
            // The login request is a POST request in which we transmit our login credentials.
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
            if (basicAuth != null) { // use basic authentication if given.
                connection.setRequestProperty("Authorization", "Basic " + basicAuth);
            }
            connection.setDoOutput(true);
            connection.connect();

            // transmit the login credentials to the server.
            OutputStream output = connection.getOutputStream();
            output.write(("{\"user\":\"" + username + "\",\"password\":\"" + password + "\"}").getBytes());
            output.flush();
            output.close();

            connection.disconnect();

            // receive the response.
            if (200 != connection.getResponseCode()) {
                throw new WebServiceClientException("could not login", connection.getResponseMessage());
            }
            // read the authentication cookie from the response.
            String setCookieHeader = connection.getHeaderField("Set-Cookie");
            String authCookie = setCookieHeader.split(";")[0];
            if (authCookie.startsWith("HxIFUser=")) { // verify that the cookie starts with the expected cookie name.
                this.authCookie = authCookie;
            }
        } catch (IOException e) {
            throw new WebServiceClientException("could not login", e);
        }
    }

	/**
	 * This method will check whether this instance of the client is currently signed in.
	 * If false then it will perform a login.
	 */
    public void loginIfNecessary() throws WebServiceClientException {
        if (authCookie == null) {
            login();
        }
    }

	/**
	 * This method requests the state over all currently existing processes.
	 */
    public InputStream requestStatus() throws WebServiceClientException {
        loginIfNecessary();
        try {
            HttpURLConnection connection = (HttpURLConnection) new URL(protocol + "://" + serverHost + "/hxif/v1/status").openConnection();
            // The login request is a POST request in which we transmit our login credentials.
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Cookie", authCookie);
            connection.setRequestProperty("Accept", "application/json");
            if (basicAuth != null) { // use basic authentication if given.
                connection.setRequestProperty("Authorization", "Basic " + basicAuth);
            }
            connection.setDoInput(true);
            connection.connect();

            // receive the response.
            if (200 != connection.getResponseCode()) {
                throw new WebServiceClientException("could not login", connection.getResponseMessage());
            }
            return connection.getInputStream();
        } catch (IOException e) {
            throw new WebServiceClientException("could not login", e);
        }
    }

	/**
	 * This method will transmit the given dataStream to the server to trigger an import of the given data.
	 */
    public InputStream transmitEmployees(final MimeTypeEncoding dataEncoding, final InputStream dataStream, final MimeTypeEncoding expectedEncoding) throws WebServiceClientException {
        loginIfNecessary();
        try {
            HttpURLConnection connection = (HttpURLConnection) new URL(protocol + "://" + serverHost + "/hxif/v1/employee").openConnection();
            // The login request is a POST request in which we transmit our login credentials.
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Cookie", authCookie);
            connection.setRequestProperty("Content-Type", dataEncoding.getMineType());
            connection.setRequestProperty("Accept", "application/xml");
            if (basicAuth != null) { // use basic authentication if given.
                connection.setRequestProperty("Authorization", "Basic " + basicAuth);
            }
            connection.setInstanceFollowRedirects(false);
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.connect();

            // transmit the given data.
            OutputStream output = connection.getOutputStream();
            byte[] buf = new byte[512];
            int bytesRead;
            while ((bytesRead = dataStream.read(buf)) > -1) {
                output.write(buf, 0, bytesRead);
            }
            output.flush();
            output.close();

            connection.disconnect();

            // receive the response.
            final int responseCode = connection.getResponseCode();
            if (responseCode != HttpURLConnection.HTTP_OK) {
                // check whether it may be a redirect.
                if (responseCode == HttpURLConnection.HTTP_MOVED_TEMP ||
                    responseCode == HttpURLConnection.HTTP_MOVED_PERM ||
                    responseCode == HttpURLConnection.HTTP_SEE_OTHER) {
                    return processRedirect(connection.getHeaderField("Location"), expectedEncoding);
                } else {
                    throw new WebServiceClientException("could not transmit employee", connection.getResponseCode() + "-" + connection.getResponseMessage());
                }
            }

            return connection.getInputStream();
        } catch (IOException e) {
            throw new WebServiceClientException("could not transmit employee", e);
        }
    }

	/**
	 * This method deals with redirects coming from the server.
	 */
    public InputStream processRedirect(final String location, final MimeTypeEncoding expectedEncoding) throws WebServiceClientException {
        loginIfNecessary();
        try {
            HttpsURLConnection connection = (HttpsURLConnection) new URL(location).openConnection();
            // The login request is a POST request in which we transmit our login credentials.
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Cookie", authCookie);
            connection.setRequestProperty("Accept", expectedEncoding.getMineType());
            if (basicAuth != null) { // use basic authentication if given.
                connection.setRequestProperty("Authorization", "Basic " + basicAuth);
            }
            connection.setInstanceFollowRedirects(false);
            connection.setDoInput(true);
            connection.connect();

            connection.disconnect();

            // receive the response.
            final int responseCode = connection.getResponseCode();
            if (responseCode != HttpURLConnection.HTTP_OK) {
                // check whether it may be a redirect.
                if (responseCode == HttpURLConnection.HTTP_MOVED_TEMP ||
                        responseCode == HttpURLConnection.HTTP_MOVED_PERM ||
                        responseCode == HttpURLConnection.HTTP_SEE_OTHER) {
                    return processRedirect(connection.getHeaderField("Location"), expectedEncoding);
                } else {
                    throw new WebServiceClientException("could not transmit employee", connection.getResponseCode() + "-" + connection.getResponseMessage());
                }
            }

            return connection.getInputStream();
        } catch (IOException e) {
            throw new WebServiceClientException("could not transmit employee", e);
        }
    }
}
